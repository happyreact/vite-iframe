
import IFramePage from './component/IframePage'

function App() {

  return (
    <>
      <h1>Vite + React</h1>
      <IFramePage />
    </>
  )
}

export default App
