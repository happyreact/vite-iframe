
// pages/UrlFrame.tsx

import { useState } from 'react';

const UrlFrame = () => {
  const [url, setUrl] = useState('');
  const [iframeSrc, setIframeSrc] = useState('');

  // Updates the URL state when user types in the input field
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setUrl(event.target.value);
  };

  // Sets the iframe source to render it when button is clicked
  const loadIframeContent = () => {
    if (url) {
      setIframeSrc(url);
    }
  };

  return (
    <div className="p-4 w-full">

      {/* Input area for URL */}
      <input
      style={{
        width:"100%",
        height:"50px",
        border: "1px solid blue",
        borderRadius:"4px",
        marginTop:"30px"

      }}
        type="text"
        name="Please input url"
        placeholder="Please input url"
        value={url}
        onChange={handleInputChange}
        className="border p-2 w-full mb-4"
      />

      {/* Button to load iframe content */}
      <button onClick={loadIframeContent} className="bg-blue-500 text-white px-4 py-2 rounded" style={{
        background:"darkblue",
        color:"white",
        marginRight:"auto",
        marginTop:"30px",
        marginBottom:"30px"
      }}>
        Load Iframe Content
      </button>

       {/* Iframe rendering area */}
       <div className={`mt-4 w-full bg-white ${!iframeSrc ? 'bg-white' : ''}`}>
           <iframe src={iframeSrc} width="100%" height="600px" frameBorder="0"></iframe>
       </div>
     </div>
   );
};

export default UrlFrame;